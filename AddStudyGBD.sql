EXEC AddStudy 'GBD'
DECLARE @StudyId INT
SELECT @StudyId=StudyId FROM Study WHERE StudyName='GBD'
EXEC AddStudyStatus @StudyId,1,'Aktiv',1
EXEC AddStudyStatus @StudyId,2,'Mors',0
EXEC AddStudyStatus @StudyId,3,'Flyttet',0
EXEC AddStudyStatus @StudyId,4,'Avsluttet',0
GO
EXEC AddSchema 'GBD'
GO
